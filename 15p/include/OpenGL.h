#if defined(linux) || defined(_WIN32)
    #include <GL/glut.h>    /*Для Linux и Windows*/
#else
    #include <GLUT/GLUT.h>  /*Для OS X*/
#endif

#include <cstring>
#include <iostream>

#ifndef OGL
#define OGL

extern int tile[4][4];
extern bool console;

class OpenGL{
  static void output(int x, int y, char *string, void *font); // printing any text
public:
  static void reshape(int w, int h); // default shit with matrix(on window init)
	static void square(int i, int j); // default shit with matrix(on window init)
	static void text(); //printing manual in right side (using output())
	static void display(); // drawing first frame
	static void win(); // drawing info that game finished (using output())
};
#endif // OGL
