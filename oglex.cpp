/*
 * Code by @anarh1st47 (Dmitry Nikolaev)
 * Colors by @eternal_search (Dasha Axenenko)
 * Licensed with WTFPL <wtfpl.net>
 */
#if defined(linux) || defined(_WIN32)
#include <GL/glut.h>    /*Для Linux и Windows*/
#else
#include <GLUT/GLUT.h>  /*Для OS X*/
#endif

#include <cstring>
#include <cstdio>
#include <iostream>

#define SWAP(a, b)   ((&(a) == &(b)) ? (a) : ((a)^=(b),(b)^=(a),(a)^=(b)))
using namespace std;

bool console = false;
int tile[4][4] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};



class OpenGL{
public:
	static void reshape(int w, int h){ // default shit with matrix(on window init)
		glViewport(0, 0, w, h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0, w, 0, h);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
private:
	static void output(int x, int y, char *string){ // printing text
	  void *font = GLUT_BITMAP_TIMES_ROMAN_24;

	  glColor3f(.36, .02, .19);
	  glRasterPos2f(x, y);
	  int len, i;
	  len = (int)strlen(string);
	  for (i = 0; i < len; i++) {
	    glutBitmapCharacter(font, string[i]);
	  }
	}
public:
	static void square(int i, int j){
		int x1 = 150 * j, y1 = 600 - 150 * i;
		//j = j + 4 * i + 1;
		glBegin(GL_QUADS);

		//2 strings for changing type from "int j" to "char buffer" (cuz itoa dont works well)
		char buffer [16];
		snprintf(buffer, sizeof(buffer), "%ld", tile[i][j]);

		if(tile[i][j] == 0){
			buffer[0] = ' ';
			glColor3f(.04, .035, .13);
		}
		else
			glColor3f(.27, .52, .9);

	  int x2 = x1 + 145, y2 = y1 - 145;
		glVertex2i(x1, y1);
		glVertex2i(x1, y2);
		glVertex2i(x2, y2);
		glVertex2i(x2, y1);
		glEnd();
		output(x1 + 60, y1 - 80, (char*)buffer);
	}
	static void text(){
		output(630, 500, (char*)"Controlling\n");
		output(635, 470, (char*)"box using");
		output(650, 440, (char*)"WSAD");
		output(620, 350, (char*)"shuffle -- space");
	}
	static void display(){
		glClear(GL_COLOR_BUFFER_BIT);

		for(int i = 0; i < 4; i++)
			for(int j = 0; j < 4; j++)
				square(i, j);
		text();
		glutSwapBuffers();
	}
};


class Hello{
	static const int cntShuffles = 80;
	int player[2] = {3, 3};

	void printTile(){
		if(!console)  glClear(GL_COLOR_BUFFER_BIT);
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				cout<<"|";
				tile[i][j] != 0 ? (tile[i][j] < 10 ? (cout<<" "<<tile[i][j]) : cout<<tile[i][j]) : cout<<"  ";
				if(!console) OpenGL::square(i, j);
			}
			cout<<"|"<<endl;
		}
		cout<<endl;
		if(!console){
			OpenGL::text();
			glutSwapBuffers(); // changing frame
		}
	}

	void go(int pos, bool p){
		if(pos == 0 && player[1] != 3){ //left
			SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] + 1]);
			player[1] += 1;
		} else if(pos == 1 && player[1] != 0){ //right
			SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] - 1]);
			player[1] -= 1;
		} else if(pos == 2 && player[0] != 3){ //up
			SWAP(tile[player[0]][player[1]], tile[player[0] + 1][player[1]]);
			player[0] += 1;
		} else if(pos == 3 && player[0] != 0){ //down
			SWAP(tile[player[0]][player[1]], tile[player[0] - 1][player[1]]);
			player[0] -= 1;
		}
		if(p)  printTile();
	}
public:
	inline void left (bool p = 1){go(0, p);} // left() more readability than go(0, 1)
	inline void right(bool p = 1){go(1, p);}
	inline void up   (bool p = 1){go(2, p);}
	inline void down (bool p = 1){go(3, p);}

	void shuffle(){
		int move;
		for(int i = 0; i < cntShuffles; i++){
			move = rand() % 4;
			if(move == 0)
				left(false);
			else if(move == 1)
				right(false);
			else if(move == 2)
				down(false);
			else if(move == 3)
				up(false);
		}
		printTile();
	}
};

Hello kek;

/*
 *Charatar controller
 */
static void keyPressed(unsigned char c, int x, int y){
	if(c == 'w')
		kek.up();
	else if(c == 'a')
		kek.left();
	else if(c == 's')
		kek.down();
	else if(c == 'd')
		kek.right();
	else if(c == ' ')
		kek.shuffle();
}

int main (int argc, char * argv[]){ // can type arg "-console"
	if(argc == 1 || strcmp(argv[1], "-console")){ //init OpenGL
		glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);

		glutInitWindowSize(800, 600);
		glutCreateWindow("15 puzzle");

		glutReshapeFunc(OpenGL::reshape);
		glutDisplayFunc(OpenGL::display);
		glutKeyboardFunc(keyPressed);

		glutMainLoop();
	} else{
		console = true;
		int c;
		do{
			c=getchar();
			keyPressed(c, 0, 0);
		} while(c != '.');
	}
	return 0;
}
