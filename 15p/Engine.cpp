#include "include/Engine.h"
#include "include/OpenGL.h"
#if defined(_WIN32)
    #define CLEAR "cls"
#else
    #define CLEAR "clear"
#endif
#define SWAP(a, b)   ((&(a) == &(b)) ? (a) : ((a)^=(b),(b)^=(a),(a)^=(b)))


OpenGL gl;

bool testEqual( const int arr[4][4]){
    for(int i = 0; i < 4; i++) for(int j=0;j<4;j++)
        if(arr[i][j] != 1 + i*4 + j && i*j != 9) // Top checker (if array == {1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 rnd})
            return false;
    return true;
}

void Engine::printTile(){
    if(testEqual(tile))
        isWin = true;
    system(CLEAR);
    if(!console)
        glClear(GL_COLOR_BUFFER_BIT);
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
            cout<<"|";
            tile[i][j] != 0 ? (tile[i][j] < 10 ? (cout<<" "<<tile[i][j]) : cout<<tile[i][j]) : cout<<"  ";
            if(!console && !isWin) gl.square(i, j);
        }
        cout<<"|"<<endl;
    }
    cout<<endl;
    if(!console){
        if(!isWin) gl.text();
        else gl.win();
        glutSwapBuffers();
    }
    else if(isWin){
      cout<<(char*)"YOU WIN!\n";
      isWin = false;
    }
    
}
void Engine::go(int pos, bool p){
    if(pos == 0 && player[1] != 3){ //left
        SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] + 1]);
        player[1] += 1;
    } else if(pos == 1 && player[1] != 0){ //right
        SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] - 1]);
        player[1] -= 1;
    } else if(pos == 2 && player[0] != 3){ //up
        SWAP(tile[player[0]][player[1]], tile[player[0] + 1][player[1]]);
        player[0] += 1;
    } else if(pos == 3 && player[0] != 0){ //down
        SWAP(tile[player[0]][player[1]], tile[player[0] - 1][player[1]]);
        player[0] -= 1;
    }
    if(p)  printTile();
}
void Engine::shuffle(){
    int move;
    for(int i = 0; i < cntShuffles; i++){
        move = rand() % 4;
        if(move == 0)
            left(false);
        else if(move == 1)
            right(false);
        else if(move == 2)
            down(false);
        else if(move == 3)
            up(false);
		}
    printTile();
}
