#ifndef ENGINE_H
#define ENGINE_H


extern bool console; // are we playing in console? if false using OpenGL
extern int tile[4][4];
extern bool isWin; // useing in charater controller and printTile()

using namespace std;

class Engine{
	static const int cntShuffles = 100;
	int player[2] = {3, 3}; // default user coordinates
	void printTile(); // Drawing all tile (OpenGL here!!))
	void go(int pos, bool p); // backend to left(). right, etc..
public:
	inline void left (bool p = 1){go(0, p);} //more readable than go(0, 1)
	inline void right(bool p = 1){go(1, p);}
	inline void up   (bool p = 1){go(2, p);}
	inline void down (bool p = 1){go(3, p);}
	void shuffle(); //method which do randon steps cntShuffle times (need to randomize tile)

};
#endif // ENGINE_H
