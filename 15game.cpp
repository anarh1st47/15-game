#include <iostream>
#define SWAP(a, b)   ((&(a) == &(b)) ? (a) : ((a)^=(b),(b)^=(a),(a)^=(b)))
using namespace std;


class Hello{
	const int cntShuffles = 80;
	int player[2] = {3, 3};
	int tile[4][4] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
	void printTile(){
		for(int i = 0; i < 4; i++){
			for(int j = 0; j < 4; j++){
				cout<<"|";
				tile[i][j] != 0 ? (tile[i][j] < 10 ? (cout<<" "<<tile[i][j]) : cout<<tile[i][j]) : cout<<"  ";;
			}
			cout<<"|"<<endl;
		}
		cout<<endl;
	}
public: 
	void say(){	
		printTile();
	}
	void up(bool p = 1){
		if(player[0] != 3){
			SWAP(tile[player[0]][player[1]], tile[player[0] + 1][player[1]]);
			player[0] += 1;
		}
		if(p)  printTile();
	}
	
	void down(bool p = 1){
		if(player[0] != 0){
			SWAP(tile[player[0]][player[1]], tile[player[0] - 1][player[1]]);
			player[0] -= 1;
		}
		if(p)  printTile();
	}
	void left(bool p = 1){
		if(player[1] != 3){
			SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] + 1]);
			player[1] += 1;
		}
		if(p)  printTile();
	}
	void right(bool p = 1){
		if(player[1] != 0){
			SWAP(tile[player[0]][player[1]], tile[player[0]][player[1] - 1]);
			player[1] -= 1;
		}
		if(p)  printTile();
	}
	void shuffle(){
		int move;
		for(int i = 0; i < cntShuffles; i++){
			move = rand() % 4;
			switch(move){
				case 0:
					left(false);
					break;
				case 1:
					right(false);
					break;
				case 2:
					down(false);
					break;
				case 3:
					up(false);
					break;
			}
			
		}
		printTile();
	}
};

int main(){
	Hello kek;
	kek.shuffle();
	//kek.say();
	int c;
	do {
		//w == 119; a == 97; s == 115; d == 100; space == 32;
	    c=getchar();
	    //putchar (c);
	    //cout<<c;
	    if(c == 119)
	    	kek.down();
	    else if(c == 97)
	    	kek.right();
	    else if(c == 115)
	    	kek.up();
	    else if(c == 100)
	    	kek.left();
	    else if(c == 32)
	    	kek.shuffle();
	} while (c != '.');
	/*kek.right();
	kek.down();
	kek.left();
	kek.up();*/
	return 0;
}
