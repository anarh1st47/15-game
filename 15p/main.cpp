/*
 * Code by @anarh1st47 (Dmitry Nikolaev)
 * Colors by @eternal_search (Dasha Axenenko)
 * Licensed with WTFPL <wtfpl.net>
 */

#include "include/OpenGL.h"
#include "include/Engine.h"

bool console = false, isWin = false;
int tile[4][4] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,0}};
Engine kek;


/*
 * Character controller
 */

bool checkButtons(const char c, const char nid){ // CapsLock checker
  if(c + 32 == nid || c == nid)
    return true;
  return false;
}


void keyPressed(unsigned char c, int x, int y){
    if(isWin && c == 'r'){
       isWin = false;
       kek.shuffle();
    }
    else{
        if(checkButtons(c, 'w'))
            kek.up();
        else if(checkButtons(c, 'a'))
            kek.left();
        else if(checkButtons(c, 's'))
            kek.down();
        else if(checkButtons(c, 'd'))
            kek.right();
        else if(c == ' ')
            kek.shuffle();
    }
}


int main (int argc, char * argv[]){ // can type arg "-console"
	if(argc == 1 || strcmp(argv[1], "-console")){
    glutInit(&argc, argv);
		glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGBA);

		glutInitWindowSize(800, 600);
		glutCreateWindow("15 puzzle");

		glutReshapeFunc (OpenGL::reshape);
		glutDisplayFunc (OpenGL::display);
		glutKeyboardFunc(keyPressed);

		glutMainLoop();
	} else {
		console = true;
		int c;
		do{
			c = getchar();
			keyPressed(c, 0, 0);
		} while(c != '.');
	}
	return 0;
}
