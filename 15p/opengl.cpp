#include "include/OpenGL.h"

void OpenGL::reshape(int w, int h){ 
		glViewport(0, 0, w, h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluOrtho2D(0, w, 0, h);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
void OpenGL::output(int x, int y, char *string, void *font = GLUT_BITMAP_TIMES_ROMAN_24){

	  glColor3f(.36, .02, .19);
	  glRasterPos2f(x, y);
	  int len, i;
	  len = (int)strlen(string);
	  for (i = 0; i < len; i++) {
	    glutBitmapCharacter(font, string[i]);
	  }
	}
void OpenGL::square(int i, int j){
		int x1 = 150 * j, y1 = 600 - 150 * i;
		glBegin(GL_QUADS);

		//2 strings for changing type from int j to char buffer
		char buffer [16];
		snprintf(buffer, sizeof(buffer), "%ld", tile[i][j]);

		if(tile[i][j] == 0){
			buffer[0] = ' ';
			glColor3f(.04, .035, .13);
		}
		else
			glColor3f(.27, .52, .9);

	  int x2 = x1 + 145, y2 = y1 - 145;
		glVertex2i(x1, y1);
		glVertex2i(x1, y2);
		glVertex2i(x2, y2);
		glVertex2i(x2, y1);
		glEnd();
		output(x1+60, y1-80, (char*)buffer);
	}

void OpenGL::text(){
		output(620, 500, (char*)"You controlling\n");
		output(620, 470, (char*)"colored box via");
		output(650, 440, (char*)"WSAD");
		output(620, 350, (char*)"shuffle -- space");
	}
void OpenGL::display(){
    glClear(GL_COLOR_BUFFER_BIT);

    for(int i = 0; i < 4; i++)
        for(int j = 0; j < 4; j++)
            square(i, j);
    text();
    glutSwapBuffers();
}
void OpenGL::win(){
    output(350, 270, (char*)"You WIN!!");
    output(335, 200, (char*)"press R to restart", GLUT_BITMAP_HELVETICA_18);
}
